package com.zhm.springboot.dao;

import com.zhm.springboot.domain.Urls;

import java.util.List;

/**
 * Created by haiming.zhuang on 2016/7/13.
 */
public interface UrlsDao {
    List<Urls> findByRole(Integer userid);

    List<Urls> findByUserid(Integer userid);
}
