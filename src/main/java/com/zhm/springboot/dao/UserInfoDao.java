package com.zhm.springboot.dao;

import com.zhm.springboot.domain.UserInfo;

import java.util.List;

/**
 * Created by haiming.zhuang on 2016/7/12.
 */
public interface UserInfoDao {
    List<UserInfo> findAll();

    UserInfo findById(Integer userid);

    void delById(Integer userid);

    void saveUser(UserInfo user);

    void updateUser(UserInfo dbInfo);

    UserInfo findByUsername(String username);
}
