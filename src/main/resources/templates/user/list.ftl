<#assign sec=JspTaglibs["http://www.springframework.org/security/tags"] />
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script>
    function doDel(userid){
        if(confirm("确认删除该数据吗？")){
            $.ajax({
                type:"POST",
                url:"/user/del",
                data:{userid:userid,${_csrf.parameterName}:'${_csrf.token}'},
                error:function(msg){
                    alert(msg.responseText);
                },
                success:function(msg){
                    alert("删除成功");
                    location.reload();
                }
            });
        }
    }
</script>
<@sec.authorize  access="hasAuthority('/user/add')">
<a href="/user/add" >新增用户</a>
</@sec.authorize>
<#if currUser?exists>
    ${currUser.username},欢迎您。
    <a href="/logout" >退出登录</a>
</#if>

<table>
    <tr>
        <th>用户名</th>
        <th>手机号</th>
        <th>注册时间</th>
    <#if currUser?exists>
        <th>操作</th>
</#if>
    </tr>
    <#list results as item>
        <tr>
            <td>${item.username}</td>
            <td>${item.telephone}</td>
            <td>${item.entry_date?string('yyyy-MM-dd HH:mm:ss')}</td>
        <#if currUser?exists>
            <td>
                <@sec.authorize  access="hasAuthority('/user/mod')">
                    <a href="/user/mod?userid=${item.id}">修改</a>
                </@sec.authorize>
                <@sec.authorize  access="hasAuthority('/user/del')">
                    <a href="javascript:doDel(${item.id})">删除</a>
                </@sec.authorize>
            </td>
        </#if>
        </tr>
    </#list>
</table>
<#if !currUser?exists>
<a href="/login">登录后操作</a>
</#if>